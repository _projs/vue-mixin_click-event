# ◦ Dependencies

```html
<script src="https://philippn.com/vendor/console/v0.0.1/dist.js"></script>
```

[Console wrapper](https://gitlab.com/_projs/console) GitLab page

# ◦ Load

### Latest
```html
<script src="https://philippn.com/vendor/vue/mixin/click-event/dist.js"></script>
```

### A certain version
```html
<script src="https://philippn.com/vendor/vue/mixin/click-event/v0.0.1/dist.js"></script>
```

### Composer (PHP Packagist)

```
composer require projs/vue-mixin_click-event
```

```html
<script src="/vendor/projs/vue-mixin_click-event/dist.js"></script>
```

### NPM @todo

# ◦ Use


```js
    new Vue({
        mixins: [ClickEventMixin]
    })
```

```vue
<div @click="click($event);"></div>
```

### Console.log()

```
Search click target by attr data-click-id <timestamp>
```