const ClickEventMixin = {
    methods: {
        click: function(event){
            var timestamp = Date.now();
            Console.log("Search click target by attr data-click-id\n", timestamp);

            event.target.setAttribute("data-click-id", timestamp);
        }
    }
}